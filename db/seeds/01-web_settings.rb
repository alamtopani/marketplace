module SeedWebSetting
  def self.seed
    WebSetting.find_or_create_by({
      header_tags: "Oshop Market",
      footer_tags: "Oshop Market",
      contact: '(0266) 234657',
      email: 'sales@oshop.com',
      facebook: 'http://www.facebook.com/',
      twitter: 'http://www.twitter.com/',
      title: "Oshop Market",
      keywords: "Oshop Market, Situs Jual beli barang",
      description: 'Etsy was founded in June 2005 in an apartment in Brooklyn, New York to fill a need for an online community where crafters, artists and makers could sell their handmade and vintage goods and craft supplies. In the spirit of handmade, founder Rob Kalin and two friends designed the first site',
      robot: 'Follow',
      author: "Oshop Market",
      corpyright: "Oshop Market",
      revisit: '2 Days',
      expires: 'Never',
      revisit_after: '2 Days',
      geo_placename: 'Indonesia',
      language: 'ID',
      country: 'ID',
      content_language: 'All-Language',
      distribution: 'global',
      generator: 'website',
      rating: 'general',
      target: 'global',
      search_engines: "Oshop Market, Jual Beli Barang"
    })
  end
end
