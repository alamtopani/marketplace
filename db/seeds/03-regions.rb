module SeedRegion
  REGIONS =  {
    "Kota Sukabumi"=> [
      "Baros"=> [
        "Baros",
        "Jayamekar",
        "Jayaraksa",
        "Sudajayahilir"
      ],
      "Cibeureum"=>[
        "Babakan",
        "Cibeureumhilir",
        "Limusnunggal",
        "Sindangpalay"
      ],
      "Cikole"=>[
        "Cikole",
        "Cisarua",
        "Gunungparang",
        "Kebonjati",
        "Selabatu",
        "Subangjaya"
      ],
      "Citamiang"=>[
        "Cikondang",
        "Citamiang",
        "Gedongpanjang",
        "Nanggeleng",
        "Tipar"
      ],
      "Gunungpuyuh"=>[
        "Gunungpuyuh",
        "Karamat",
        "Karangtengah",
        "Sriwedari"
      ],
      "Lembursitu"=>[
        "Cikundul",
        "Cipanengah",
        "Lembursitu",
        "Sindangsari",
        "Situmekar"
      ],
      "Warudoyong"=>[
        "Benteng",
        "Dayeuhluhur",
        "Nyomplong",
        "Sukakarya",
        "Warudoyong"
      ],
    ],

    "Kabupaten Sukabumi"=> [
      "Kecamatan Bantargadung"=>[
        "Bantargadung",
        "Bantargebang",
        "Bojonggaling",
        "Boyongsari",
        "Buanajaya",
        "Limusnunggal",
        "Mangunjaya",
      ],
      "Kecamatan Bojong Genteng"=>[
        "Berekah",
        "Bojong Galing",
        "Bojong Genteng",
        "Cibodas",
        "Cipanengah",
      ],
      "Kecamatan Caringin"=>[
        "Caringin Kulon",
        "Caringin Wetan",
        "Cijengkol",
        "Cikembang",
        "Mekarjaya",
        "Pasir Datar Indah",
        "Seuseupan",
        "Sukamulya",
        "Talaga"
      ],
      "Kecamatan Ciambar"=>[
        "Ciambar",
        "Cibunarjaya",
        "Ginanjar",
        "Munjul",
      ],
      "Kecamatan Cibadak"=>[
        "Batununggal",
        "Cibadak",
        "Ciheulang Tonggoh",
        "Karangtengah",
        "Neglasari",
        "Pamuruyan",
        "Sekarwangi",
        "Sukasirna",
        "Tenjojaya",
        "Warnajati"
      ],
      "Kecamatan Cibitung"=>[
        "Banyumurni",
        "Banyuwangi",
        "Cibitung",
        "Cibodas",
        "Cidahu",
        "Talagamurni"
      ],
      "Kecamatan Cicantayan"=>[
        "Cicantayan",
        "Cijalingan",
        "Cimahi",
        "Cisande",
        "Hegarmanah",
        "Lembursawah",
        "Sukadamai"
      ],
      "Kecamatan Cicurug"=>[
        "Bangbayang",
        "Benda",
        "Caringin",
        "Cicurug",
        "Cisaat",
        "Kutajaya",
        "Mekarsari",
        "Nanggerang",
        "Nyangkowek",
        "Pasawahan",
        "Purwasari",
        "Tenjoayu",
        "Tenjolaya",
      ],
      "Kecamatan Cidadap"=>[
        "Banjarsari",
        "Cidadap",
        "Hegarmulya",
        "Padasenang",
      ],
      "Kecamatan Cidahu"=>[
        "Babakan Pari",
        "Cidahu",
        "Girijaya",
        "Jayabakti",
        "Pasirdoton",
        "Pondok Kaso Tengah",
        "Pondok Kaso Tonggoh",
        "Tangkil"
      ],
      "Kecamatan Cidolog"=>[
        "Cidolog",
        "Cikarang",
        "Cipamingkis",
        "Mekarjaya",
        "Tegallega"
      ],
      "Kecamatan Ciemas"=>[
        "Cibenda",
        "Ciemas",
        "Ciwaru",
        "Girimukti",
        "Mandrajaya",
        "Mekarjaya",
        "Mekarsakti",
        "Tamanjaya"
      ],
      "Kecamatan Cikakak"=>[
        "Cikakak",
        "Cileungsing",
        "Cimaja",
        "Gandasoli",
        "Margalaksana",
        "Ridogalih",
        "Sirnarasa",
        "Sukamaju"
      ],
      "Kecamatan Cikembar"=>[
        "Bojong",
        "Bojongkembar",
        "Cibatu",
        "Cikembar",
        "Cimanggu",
        "Kertaraharja",
        "Parakanlima",
        "Sukamaju",
        "Sukamulya"
      ],
      "Kecamatan Cikidang"=>[
        "Bumisari",
        "Cicareuh",
        "Cijambe",
        "Cikarae Toyyibah",
        "Cikidang",
        "Cikiray",
        "Gunung Malang",
        "Mekarnangka",
        "Nangkakoneng",
        "Pangkalan",
        "Sampora",
        "Tamansari"
      ],
      "Kecamatan Cimanggu"=>[
        "Boregah Indah",
        "Cimanggu",
        "Karangmekar",
        "Sukajadi",
        "Sukamaju",
        "Sukamanah",
      ],
      "Kecamatan Ciracap"=>[
        "Cikangkung",
        "Ciracap",
        "Gunungbatu",
        "Mekarsari",
        "Pangumbahan",
        "Pasirpanjang",
        "Purwasedar",
        "Ujung Genteng"
      ],
      "Kecamatan Cireunghas"=>[
        "Bencoy",
        "Cikurutug",
        "Cipurut",
        "Cireunghas",
        "Tegalpanjang"
      ],
      "Kecamatan Cisaat"=>[
        "Nagrak",
        "Sukasari",
        "Babakan",
        "Cibatu",
        "Cibolang Kaler",
        "Cisaat",
        "Gunungjaya",
        "Kutasirna",
        "Padaasih",
        "Selajambe",
        "Sukamanah",
        "Sukamantri",
        "Sukaresmi"
      ],
      "Kecamatan Cisolok"=>[
        "Caringin",
        "Cicadas",
        "Cikahuripan",
        "Cikelat",
        "Cisolok",
        "Gunung Kramat",
        "Gunung Tanjung",
        "Karangpapak",
        "Pasirbaru",
        "Sirnaresmi",
        "Sukarame"
      ],
      "Kecamatan Curugkembar"=>[
        "Bojong Tugu",
        "Cimenteng",
        "Curugkembar",
        "Mekartanjung",
        "Sindangraja",
        "Tanjungsari"
      ],
      "Kecamatan Geger Bitung"=>[
        "Caringin",
        "Buniwangi",
        "Ciengang",
        "Cijurey",
        "Gegerbitung",
        "Karangjaya",
        "Sukamanah"
      ],
      "Kecamatan Gunung Guruh"=>[
        "Cibentang",
        "Cibolang",
        "Cikujang",
        "Gunung Guruh",
        "Kebonmanggu",
        "Mangkalaya",
        "Sirnaresmi"
      ],
      "Kecamatan Jampang Kulon"=>[
        "Bojong Genteng",
        "Bojongsari",
        "Cikarang",
        "Ciparay",
        "Jampang Kulon",
        "Karanganyar",
        "Mekarjaya",
        "Nagraksari",
        "Padajaya",
        "Tanjung"
      ],
      "Kecamatan Jampang Tengah"=>[
        "Bantar Agung",
        "Bantarpanjang",
        "Bojong Jengkol",
        "Bojongtipar",
        "Cijulang",
        "Jampang Tengah",
        "Nangerang",
        "Padabeunghar",
        "Panumbangan",
        "Sindangresmi",
        "Tanjungsari"
      ],
      "Kecamatan Kabandungan"=>[
        "Cianaga",
        "Cihamerang",
        "Cipeuteuy",
        "Kabandungan",
        "Mekarjaya",
        "Tugubandung"
      ],
      "Kecamatan Kadudampit"=>[
        "Cikahuripan",
        "Cipetir",
        "Citamiang",
        "Gede Pangrango",
        "Kadudampit",
        "Muaradua",
        "Sukamaju",
        "Sukamanis",
        "Undrus Binangun"
      ],
      "Kecamatan Kalapa Nunggal"=>[
        "Gunung Endut",
        "Kadununggal",
        "Kalapa Nunggal",
        "Makasari",
        "Palasari Girang",
        "Pulosari",
        "Walangsari"
      ],
      "Kecamatan Kali Bunder"=>[
        "Balekambang",
        "Bojong",
        "Cimahpar",
        "Kalibunder",
        "Mekarwangi",
        "Sekarsari",
        "Sukaluyu"
      ],
      "Kecamatan Kebonpedes"=>[
        "Bojongsawah",
        "Cikaret",
        "Jambenenggang",
        "Kebonpedes",
        "Sasagaran",
      ],
      "Kecamatan Lengkong"=>[
        "Cilangkap",
        "Langkapjaya",
        "Lengkong",
        "Neglasari",
        "Tegallega"
      ],
      "Kecamatan Nagrak"=>[
        "Babakan Panjang",
        "Balekambang",
        "Cihanyawar",
        "Cisarua",
        "Darmareja",
        "Girijaya",
        "Kalaparea",
        "Nagrak Selatan",
        "Nagrak Utara",
        "Pawenang",
        "Wangunjaya"
      ],
      "Kecamatan Nyalindung"=>[
        "Bojongkalong",
        "Bojongsari",
        "Cijangkar",
        "Cisitu",
        "Kertaangsana",
        "Mekarsari",
        "Neglasari",
        "Nyalindung",
        "Sukamaju",
        "Wangunreja"
      ],
      "Kecamatan Pabuaran"=>[
        "Bantarsari",
        "Cibadak",
        "Ciwalat",
        "Lembursawah",
        "Pabuaran",
        "Sirnasari",
        "Sukajaya"
      ],
      "Kecamatan Parakan Salak"=>[
        "Bojongasih",
        "Bojonglongok",
        "Lebaksari",
        "Parakansalak",
        "Sukakersa",
        "Sukatani"
      ],
      "Kecamatan Parung Kuda"=>[
        "Babakanjaya",
        "Bojong Kokosan",
        "Kompa",
        "Langensari",
        "Palasari Hilir",
        "Parung Kuda",
        "Pondok Kaso Landeuh",
        "Sundawenang"
      ],
      "Kecamatan Palabuhan Ratu"=>[
        "Buniwangi",
        "Cibodas",
        "Cikadu",
        "Citarik",
        "Pasirsuren",
        "Pelabuhanratu",
        "Tonjong",
        "Citepus"
      ],
      "Kecamatan Purabaya"=>[
        "Cicukang",
        "Cimerang",
        "Citamiang",
        "Margaluyu",
        "Neglasari",
        "Pagelaran",
        "Purabaya"
      ],
      "Kecamatan Sagaranten"=>[
        "Cibaregbeg",
        "Cibitung",
        "Curugluhur",
        "Datarnangka",
        "Gunung Bentang",
        "Hegarmanah",
        "Margaluyu",
        "Pasanggrahan",
        "Puncakmanggis",
        "Sagaranten",
        "Sinar Bentang"
      ],
      "Kecamatan Simpenan"=>[
        "Cibuntu",
        "Cidadap",
        "Cihaur",
        "Kertajaya",
        "Loji",
        "Mekarasih"
      ],
      "Kecamatan Sukabumi"=>[
        "Karawang",
        "Parungseah",
        "Perbawati",
        "Sudajaya Girang",
        "Sukajaya",
        "Warnasari"
      ],
      "Kecamatan Sukalarang"=>[
        "Cimangkok",
        "Prianganjaya",
        "Semplak",
        "Sukalarang",
        "Sukamaju",
        "Titisan"
      ],
      "Kecamatan Sukaraja"=>[
        "Cisarua",
        "Langensari",
        "Limbangan",
        "Margaluyu",
        "Pasirhalang",
        "Selaawi",
        "Selawangi",
        "Sukamekar",
        "Sukaraja"
      ],
      "Kecamatan Surade"=>[
        "Buniwangi",
        "Cipeundeuy",
        "Citanglar",
        "Gunung Sungging",
        "Jagamukti",
        "Kadaleman",
        "Pasiripis",
        "Sirnasari",
        "Sukatani",
        "Swakarya",
        "Wanasari"
      ],
      "Kecamatan Tegal Buleud"=>[
        "Bangbayang",
        "Buniasih",
        "Calingcing",
        "Nangela",
        "Rambay",
        "Sirnamekar",
        "Sumberjaya",
        "Tegalbuleud"
      ],
      "Kecamatan Waluran"=>[
        "Caringin Nunggal",
        "Mekar Mukti",
        "Sukamukti",
        "Waluran",
        "Waluran Mandiri"
      ],
      "Kecamatan Warung Kiara"=>[
        "Bantar Gebang",
        "Bantarkalong",
        "Bojongkerta",
        "Damarraja",
        "Girijaya",
        "Hegarmanah",
        "Sirnajaya",
        "Sukaharja",
        "Tarisi",
        "Ubrug",
        "Warungkiara"
      ]
    ]
  }

  def self.seed
    REGIONS.keys.each do |city_name|
      city = Region.find_or_initialize_by(name: city_name)
      city.name = city_name
      city.save
      
      REGIONS[city_name][0].keys.each do |district_name|
        district = Region.new
        district.name = district_name
        district.parent = city
        district.save

        REGIONS[city_name][0][district_name].each do |village_name|
          village = Region.new
          village.name = village_name
          village.parent = district
          village.save
        end 
      end 
    end  
  end
end