module SeedCategory
  CATEGORIES = {
    "Fashion Aksesoris" => [
      "Tas Pria dan Wanita",
      "Sepatu",
      "Aksesoris",
      "Aksesoris Rambut",
      "Perhiasan",
      "Jam Tangan",
      "Barang Couple",
      "Fashion dan Aksesoris Lainnya"
    ],

    "Pakaian" => [
      "Pakaian Wanita",
      "Pakaian Pria",
      "Pakaian Anak Perempuan",
      "Pakaian Anak Laki-Laki",
      "Kaos",
      "Baju Korea",
      "Batik",
      "Baju Muslim"
    ],

    "Kecantikan" => [
      "Kosmetik",
      "Perawatan Wajah",
      "Perawatan Tangan, Kaki dan Kuku",
      "Perawatan Rambut",
      "Perawatan Mata",
      "Mandi & Perawatan Tubuh"
    ],

    "Kesehatan" => [
      "Kesehatan Wanita",
      "Obat & Alat Kesehatan",
      "Kesehatan Gigi & Mulut",
      "Diet & Vitamin",
      "Kesehatan Lainnya"
      
    ],

    "Rumah Tangga" => [
      "Kamar Tidur",
      "Kamar Mandi",
      "Laundry",
      "Kebersihan",
      "Ledeng",
      "Tempat Penyimpanan",
      "Furnitur",
      "Dekorasi",
      "Alat Pertukangan",
      "Rumah Tangga Lainnya"

    ],

    "Dapur" => [
      "Peralatan Dapur",
      "Peralatan Masak",
      "Pisau",
      "Food & Drink Maker",
      "Peralatan Makan",
      "Bekal",
      "Penyimpanan Makan",
      "Dapur Lainnya",
      "Alat Pertukangan",
      "Rumah Tangga Lainnya"

    ], 

    "Perawatan Bayi" => [
      "Baju & Sepatu Bayi",
      "Perlengkapan Bayi",
      "Makanan & Susu Bayi"
      
    ],

    "Handphone & Tablet" => [
      "Handphone",
      "Aksesoris Handphone",
      "Tablet",
      "Memory Card",
      "Kabel Data",
      "Power Bank",
      "Kabel & Konektor",
      "Headset Bluetoth",
      "No Perdana & Voucher",
      "Stylus",
      "Lensa Handphone",
      "Smartwatch"
    ],

    "Laptop & Aksesoris" => [
      "Laptop",
      "Aksesoris Laptop"
      
    ],

    "Komputer & Aksesoris" => [
      "Desktop & Mini PC",
      "Komponen Komputer",
      "Optical Drive",
      "VGA Card",
      "Harddisk & Flashdisk",
      "Printer",
      "Networking",
      "Peripheral & Aksesoris"
      
    ],

    "Elektronik" => [
      "Audio",
      "TV",
      "Kamera Pengintai",
      "Media Player",
      "Telepon",
      "Game Console",
      "Tool & Kit",
      "Pencahayaan",
      "Listrik",
      "Perangkat Elektronik Lainnya"
    ],

    "Kamera, Foto & Video" => [
      "Kamera",
      "Lensa & Aksesoris",
      "Flash & Aksesoris",
      "Baterai, Charger & Grip",
      "Cleaner & Tool Kit",
      "Aksesoris Kamera",
      "Roll Film & Cetak Foto",
      "Frame & Album"
    ],

    "Otomotif" => [
      "Spare Part Mobil",
      "Spare Part Motor",
      "Aksesoris Mobil",
      "Aksesoris Motor",
      "Helm Motor",
      "Aksesoris Pengendara Motor",
      "Oil & Penghemat BBM",
      "Perawatan Kendaraan"
    ],

    "Olahraga" => [
      "Sepak Bola & Futsal",
      "Basket",
      "Badminton",
      "Tenis",
      "Sepeda",
      "Renang & Diving",
      "Alat Pancing",
      "Outdor Sport",
      "Airsoft Gun",
      "Alat Tembak & Panahan",
      "Beladiri",
      "Gym & Fitness",
      "Aksesoris Olahraga",
      "Olahraga Lainnya"
    ],

    "Office & Stationery" => [
      "Alat Tulis",
      "Alat Menggambar",
      "Kertas",
      "Surat-Menyurat",
      "Pengikat & Perekat",
      "Alat Pemotong Kertas",
      "Dokument Organizer",
      "Kalkulator & Kamus Elektronik",
      "Office & Stationery Lainnya"
    ],

    "Souvenir, Kado & Hadiah" => [
      "Boneka",
      "Kerajinan Tangan",
      "Celangan",
      "Gantungan Kunci",
      "Korek Api",
      "Asbak",
      "Bungkus Kado",
      "Kartu Ucapan",
      "Perlengkapan Pesta",
      "Souvenir, Kado & Hadiah Lainnya"
    ],

    "Mainan & Hobi" => [
      "Figure",
      "Model Kit",
      "Diecast",
      "Mainan Remote Control",
      "Mainan Anak",
      "Mainan & Hobi Lainnya"
    ],

    "Makanan & Minuman" => [
      "Biskuit & Kue",
      "Makanan Manis",
      "Makanan Kering",
      "Minuman",
      "Makanan & Minuman Kesehatan",
      "Makanan Siap Saji",
      "Bumbu & Bahan Dasar",
      "Kalkulator & Kamus Elektronik",
      "Office & Stationery Lainnya", 
      "Makanan & Minuman Lainnya"
    ],

    "Buku" => [
      "Novel & Sastra",
      "Buku Remaja dan Anak",
      "Sosial Politik",
      "Hobi",
      "Majalah",
      "Arsitektur & Desain",
      "Pengetahuan",
      "Agama & Filsafat",
      "Komputer & Internet", 
      "Kedokteran", 
      "Kewanitaan",
      "Pengembangan Diri & Karir",
      "Buku Sekolah",
      "Buku Import",
      "Buku Lainnya"
    ],

    "Film, Musik & Game" => [
      "Movie & Serial TV",
      "Musik",
      "Games"
    ]
  }

  def self.seed
    CATEGORIES.keys.each_with_index do |category_root, index|
      category = Category.find_or_initialize_by(name: category_root)
      category.name = category_root
      category.description = 'look on our impressive collections and choose your desired item, look on our impressive collections and choose your desired item'
      category.status = true
      category.file = File.open("#{Rails.root}/db/assets/icon/#{category_root}.png")
      category.save

      CATEGORIES[category.name].each do |c|
        child = Category.new
        child.parent = category
        child.name = c
        child.status = true
        child.save
      end
    end
  end
end
