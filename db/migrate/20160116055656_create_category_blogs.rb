class CreateCategoryBlogs < ActiveRecord::Migration
  def change
    create_table :category_blogs do |t|
      t.string :slug
      t.string :name
      t.string :ancestry
      t.text :description
      t.attachment :cover

      t.timestamps null: false
    end
  end
end
