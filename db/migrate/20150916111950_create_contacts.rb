class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string :phone
      t.string :handphone
      t.string :pin
      t.string :facebook
      t.string :twitter
      t.string :website
      t.string :contactable_type
      t.integer :contactable_id

      t.timestamps null: false
    end
  end
end
