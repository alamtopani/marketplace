class AddImageAndDescriptionInCategory < ActiveRecord::Migration
  def self.up
    change_table :categories do |t|
      t.attachment :file
      t.text       :description
      t.boolean    :status, default: false
    end
  end

  def self.down
    drop_attached_file :categories, :file
    remove_column :categories, :description, :text
    remove_column :categories, :status, :boolean
  end
end
