class AddBasicPriceInProduct < ActiveRecord::Migration
  def change
    add_column :products, :basic_price, :decimal
  end
end
