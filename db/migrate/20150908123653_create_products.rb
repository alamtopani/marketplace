class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :code
      t.string :slug
      t.integer :user_id
      t.string :title
      t.decimal :price
      t.text :description
      t.boolean :status, default: false
      t.integer :category_id
      t.boolean :featured, default: false
      t.datetime :start_at
      t.datetime :end_at

      t.timestamps null: false
    end

    add_index :products, :user_id
    add_index :products, :category_id
  end
end
