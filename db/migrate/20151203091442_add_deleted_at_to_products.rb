class AddDeletedAtToProducts < ActiveRecord::Migration
  def change
    add_column :products, :deleted_at, :datetime
    add_index :products, :deleted_at

    add_column :galleries, :deleted_at, :datetime
    add_index :galleries, :deleted_at

    add_column :product_specs, :deleted_at, :datetime
    add_index :product_specs, :deleted_at
  end
end
