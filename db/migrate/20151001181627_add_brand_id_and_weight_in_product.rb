class AddBrandIdAndWeightInProduct < ActiveRecord::Migration
  def change
    add_column :products, :brand_id, :integer
    add_column :products, :weight, :decimal
    add_index :products, :brand_id
  end
end
