class AddRangeShippingInProductSpecs < ActiveRecord::Migration
  def change
    add_column :product_specs, :range_shipping, :string
  end
end
