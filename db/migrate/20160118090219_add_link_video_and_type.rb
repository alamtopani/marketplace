class AddLinkVideoAndType < ActiveRecord::Migration
  def change
    add_column :blogs, :link_video, :string
    add_column :blogs, :type_blog, :string
    add_column :blogs, :sources, :string
  end
end
