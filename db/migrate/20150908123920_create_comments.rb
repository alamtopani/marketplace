class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.integer :user_id
      t.string :commentable_type
      t.integer :commentable_id
      t.text :comment
      t.boolean :status, default: false

      t.timestamps
    end

    add_index :comments, :user_id
    add_index :comments, :commentable_type
    add_index :comments, :commentable_id
  end
end
