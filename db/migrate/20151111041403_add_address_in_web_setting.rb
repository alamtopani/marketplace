class AddAddressInWebSetting < ActiveRecord::Migration
  def change
    add_column :web_settings, :address, :text
    add_column :web_settings, :longitude, :string
    add_column :web_settings, :latitude, :string
  end
end
