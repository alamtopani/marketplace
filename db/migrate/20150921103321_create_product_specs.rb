class CreateProductSpecs < ActiveRecord::Migration
  def change
    create_table :product_specs do |t|
      t.integer :product_id
      t.integer :brand_id
      t.text :information
      t.text :material
      t.integer :stock, default: 0
      t.string :available_size

      t.timestamps null: false
    end

    add_index :product_specs, :brand_id
    add_index :product_specs, :product_id
  end
end
