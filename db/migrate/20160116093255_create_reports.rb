class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.string :category
      t.string :name
      t.string :email
      t.text :message
      t.boolean :status
      t.integer :reportable_id
      t.string :reportable_type
      t.string :code

      t.timestamps null: false
    end
  end
end
