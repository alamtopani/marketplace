class CreateTestimonials < ActiveRecord::Migration
  def change
    create_table :testimonials do |t|
      t.integer :user_id
      t.text :message
      t.boolean :status, default: false

      t.timestamps null: false
    end

    add_index :testimonials, :user_id
  end
end
