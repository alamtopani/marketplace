class CreateGuestBooks < ActiveRecord::Migration
  def change
    create_table :guest_books do |t|
      t.string :name
      t.string :email
      t.string :phone
      t.text :description
      t.boolean :status

      t.timestamps null: false
    end
  end
end
