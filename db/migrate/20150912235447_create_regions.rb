class CreateRegions < ActiveRecord::Migration
  def change
    create_table :regions do |t|
      t.string :name
      t.string :ancestry
      t.boolean :featured, default: false
      t.attachment :image

      t.timestamps null: false
    end
  end
end
