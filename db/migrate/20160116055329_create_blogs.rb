class CreateBlogs < ActiveRecord::Migration
  def change
    create_table :blogs do |t|
      t.string :slug
      t.string :title
      t.text :description
      t.integer :category_id
      t.integer :user_id
      t.attachment :cover
      t.boolean :status

      t.timestamps null: false
    end

    add_index :blogs, :user_id
    add_index :blogs, :category_id
  end
end
