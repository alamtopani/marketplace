class CreateMerchantInfos < ActiveRecord::Migration
  def change
    create_table :merchant_infos do |t|
      t.integer :user_id
      t.string :title
      t.attachment :banner
      t.text :description
      t.text :policies

      t.timestamps null: false
    end

    add_index :merchant_infos, :user_id
  end
end
