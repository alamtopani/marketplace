class AddFavoriteableInFavorite < ActiveRecord::Migration
  def change
    add_column :favorites, :favoriteable_type, :string
    add_column :favorites, :favoriteable_id, :integer
  end
end
