class AddPoliciesInformation < ActiveRecord::Migration
  def change
    add_column :merchant_infos, :payment, :text
    add_column :merchant_infos, :shipping, :text
    add_column :merchant_infos, :refund_and_exchange, :text
    add_column :merchant_infos, :faq, :text
  end
end
