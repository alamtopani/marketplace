class AddSubCategoryId < ActiveRecord::Migration
  def change
    add_column :products, :sub_category_id, :integer, index: true
  end
end
