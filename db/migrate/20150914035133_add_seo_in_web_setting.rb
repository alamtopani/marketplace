class AddSeoInWebSetting < ActiveRecord::Migration
  def change
    add_column :web_settings, :robot, :string
    add_column :web_settings, :author, :string
    add_column :web_settings, :corpyright, :string
    add_column :web_settings, :revisit, :string
    add_column :web_settings, :expires, :string
    add_column :web_settings, :revisit_after, :string
    add_column :web_settings, :geo_placename, :string
    add_column :web_settings, :language, :string
    add_column :web_settings, :country, :string
    add_column :web_settings, :content_language, :string
    add_column :web_settings, :distribution, :string
    add_column :web_settings, :generator, :string
    add_column :web_settings, :rating, :string
    add_column :web_settings, :target, :string
    add_column :web_settings, :search_engines, :text
  end
end
