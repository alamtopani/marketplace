module Permitable
	USER = [
      :id,
      :type,
      :username,
      :email,
      :password,
      :password_confirmation,
      :provider,
      :uid,
      :oauth_token,
      :oauth_expires_at,
      :slug,
      :featured,
      :verified
    ]

  ADDRESS = {
      address_attributes: [
        :id,
        :address,
        :village,
        :district,
        :city,
        :province,
        :country,
        :postcode,
        :addressable_type,
        :addressable_id,
        :addressable,
        :longitude,
        :latitude
      ]
  }

  PROFILE = {
      profile_attributes: [
        :id,
        :user_id,
        :full_name,
        :birthday,
        :gender,
        :phone,
        :avatar,
        :cover,
        :meta_title,
        :meta_keywords,
        :meta_description
      ]
  }

  CONTACT = {
    contact_attributes: [
      :id,
      :phone,
      :handphone,
      :pin,
      :facebook,
      :twitter,
      :contactable_type,
      :contactable_id,
      :contactable,
      :website
    ]
  }

  CATEGORY = [
    :id,
    :name,
    :ancestry,
    :parent_id,
    :file,
    :description,
    :status
  ]

  FAQ = {
    faq_attributes: [
      :id,
      :payment,
      :shipping,
      :refund_and_exchange,
      :faq,
      :faqable_id,
      :faqable_type,
      :faqable,
      :_destroy
    ]
  }

  GUEST_BOOK = [
    :id,
    :name,
    :email,
    :phone,
    :description,
    :status,
  ]

  MERCHANT_INFO = {
    merchant_info_attributes: [
      :id,
  		:user_id,
      :title,
      :banner,
      :description,
      :policies,
      :payment,
      :shipping,
      :refund_and_exchange,
      :faq
    ]
  }

  PAYMENT = {
    payments_attributes: [
      :id,
      :name,
      :logo,
      :bank_name,
      :account_number,
      :paymentable_id,
      :paymentable_type,
      :paymentable,
      :position,
      :_destroy
    ]
  }

  PRODUCT = [
      :id,
      :slug,
      :brand_id,
      :user_id,
      :title,
      :description,
      :price,
      :status,
      :category,
      :category_id,
      :sub_category_id,
      :featured,
      :weight,
      :indent,
      :basic_price,
      :start_at,
      :end_at,
      :deleted_at
  ]

  GALLERY = {
      galleries_attributes: [
        :id,
        :title,
        :description,
        :file,
        :galleriable_type,
        :galleriable_id,
        :galleriable,
        :position,
        :featured,
        :_destroy,
        :deleted_at
      ]
  }

  REGION = [
    :id,
    :name,
    :ancestry,
    :featured,
    :parent_id,
    :image
  ]

  REPORT = [
    :id,
    :category,
    :name,
    :email,
    :message,
    :status,
    :reportable_id,
    :reportable_type,
    :code
  ]

  COMMENT = [
      :id,
      :user_id,
      :commentable_type,
      :commentable_id,
      :commentable,
      :comment,
      :status
  ]

  FAVORITE = [
      :user_id,
      :product_id,
      :favoriteable,
      :favoriteable_id,
      :favoriteable_type
  ]


  INQUIRY = [
    :user_id,
    :name,
    :email,
    :phone,
    :message,
    :inquiriable_type,
    :inquiriable_id,
    :inquiriable,
    :status,
    :ancestry,
    :parent_id,
    :owner_id
  ]

  LANDING_PAGE = [
      :title,
      :slug,
      :description,
      :status,
      :category
  ]

  SUBSCRIBE = [
      :email,
      :status
  ]

  TESTIMONIAL = [
      :user_id,
      :message,
      :status
  ]

  BLOG = [
    :slug,
    :title,
    :description,
    :category_id,
    :user_id,
    :cover,
    :status,
    :featured,
    :view_count,
    :link_video,
    :type_blog,
    :sources
  ]

  CATEGORY_BLOG = [
    :slug,
    :name,
    :parent_id,
    :ancestry,
    :description,
    :cover
  ]

  WEB_SETTING = [
      :id,
      :header_tags,
      :footer_tags,
      :contact,
      :email,
      :favicon,
      :logo,
      :facebook,
      :twitter,
      :title,
      :keywords,
      :description,
      :robot,
      :author,
      :corpyright,
      :revisit,
      :expires,
      :revisit_after,
      :geo_placename,
      :language,
      :country,
      :content_language,
      :distribution,
      :generator,
      :rating,
      :target,
      :search_engines,
      :address,
      :longitude,
      :latitude
  ]


  def self.controller(name)
    self.send name.gsub(/\W/,'_').singularize.downcase
  end

  # -----------FOR BACKEND------------

  def self.backend_user
    USER.dup.push(PROFILE.dup).push(ADDRESS.dup).push(CONTACT.dup).push(MERCHANT_INFO.dup)
  end

  def self.backend_admin
    backend_user
  end

  def self.backend_member
    backend_user
  end

  def self.backend_merchant
    backend_user
  end

  def self.backend_category
  	CATEGORY
  end

  def self.backend_product
    PRODUCT.dup.push(GALLERY.dup).push(ADDRESS.dup)
  end

  def self.backend_region
    REGION
  end

  def self.backend_report
    REPORT
  end

  def self.backend_comment
    COMMENT
  end

  def self.backend_landing_page
    LANDING_PAGE
  end

  def self.backend_subscribe
    SUBSCRIBE
  end

  def self.backend_testimonial
    TESTIMONIAL
  end

  def self.backend_blog
    BLOG.dup.push(GALLERY.dup)
  end

  def self.backend_category_blog
    CATEGORY_BLOG
  end

  def self.backend_web_setting
    WEB_SETTING.dup.push(GALLERY.dup).push(FAQ.dup)
  end

  # -----------FOR USER------------

  def self.userpage_member
    backend_user
  end

  def self.userpage_merchant
    backend_merchant
  end

  def self.userpage_product
    backend_product
  end

  def self.userpage_favorite
    backend_favorite
  end

  def self.userpage_inquiry
    INQUIRY
  end

  # -----------FOR FRONTEND------------

  def self.inquiry
    INQUIRY
  end

  def self.comment
    COMMENT
  end

  def self.testimonial
    backend_testimonial
  end

  def self.subscribe
    SUBSCRIBE
  end

  def self.guest_book
    GUEST_BOOK
  end

  def self.report
    REPORT
  end

end
