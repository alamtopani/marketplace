module ApplicationHelper
  def bootstrap_class_for flash_type
    { success: "alert-success", error: "alert-danger", errors: "alert-danger", alert: "alert-warning", notice: "alert-info" }[flash_type.to_sym] || flash_type.to_s
  end

  def flash_messages(opts = {})
    flash.each do |msg_type, message|
      concat(content_tag(:div, message, class: "alert #{bootstrap_class_for(msg_type)}
        alert-dismissible", role: "alert") do
        if flash[:errors].present?
          message.each do |word|
            concat(content_tag(:button, class: 'close', data: { dismiss: 'alert' }) do
              concat content_tag(:span, 'Close', class: 'sr-only')
            end)
            concat "<li>#{word}</li >".html_safe
          end
        else
          concat message
        end
      end)
    end
    nil
  end

  def active_sidebar?(controller, *actions)
    if actions.include?(params[:action].to_sym) || actions.include?(:all)
      return 'open'
    end if controller_name == controller.to_s
  end

  def active_sidebar_active?(controller, *actions)
    if actions.include?(params[:action].to_sym) || actions.include?(:all)
      return 'active'
    end if controller_name == controller.to_s
  end

  def checking_wishlist(id, current_user, url)
    checking = Favorite.where(favoriteable_id: id, favoriteable_type: 'Product', user_id: current_user)

    if checking.present?
      return '<i class="fa fa-star set active"></i>'
    else
      return "<i class='fa fa-star set click-wishlist' url='#{url}'></i>"
    end
  end

  def user_edit_url(type, current_user)
    if type == 'Member'
      edit_userpage_member_path(current_user)
    elsif type == 'Merchant'
      edit_userpage_merchant_path(current_user)
    end
  end

  def get_currency(number)
    number = 0 if number.blank?
    number_to_currency(number, :unit => "Rp ", :separator => ",", :delimiter => ".", precision: 0)
  end

  def sort_by
    @sort_by ||=
    [
      ["Newest Items", "latest", data: { url: "#{products_url(params.merge(sort_by: 'latest'))}" }],
      ["Oldest Items", "oldest", data: { url: "#{products_url(params.merge(sort_by: 'oldest'))}" }],
      ["Price: low to high", "low_to_high", data: { url: "#{products_url(params.merge(sort_by: 'low_to_high'))}" }],
      ["Price: high to low", "high_to_low", data: { url: "#{products_url(params.merge(sort_by: 'high_to_low'))}" }]
    ]
  end

  def get_child_categories(category)
    category = Category.find_by(name: category).children.alfa
  end 

  def youtube_link(url)
    if url.present?
      code = url.split("?v=")[1].split("&")[0]
      "http://www.youtube.com/embed/#{code}?feature=player_detailpage"
    end
  end

  def imdb_style_rating(rateable_obj, dimension, options = {})
    #TODO: add option to change the star icon
    overall_avg = rateable_obj.calculate_overall_average_dimension(dimension)

    content_tag :div, '', :style => "background-image:url('#{image_path('big-star.png')}');width:81px;height:81px;margin-top:10px;" do
        content_tag :p, overall_avg, :style => "position:relative;line-height:110px;text-align:center;"
    end
  end

  def rating_user(stars=0)
    html = ''
    split_star = stars.to_s.split('.')
    full_star = split_star.first.to_i
    half_star = split_star.last.to_i
    full_star.times do |i|
      html += "<img alt='#{i}' src='/assets/star-on.png'> &nbsp;"
    end
    html += "<img alt='half' src='/assets/star-half.png'> &nbsp;" if half_star != 0
    return html.html_safe
  end
end
