$(document).ready(function() {
  $(".select2").select2({
  	maximumSelectionLength: 2
  });

  $('.choice-gallery-form .input-file').on('change', function(index){
    if($(this).length){
      previewFile(this, $(this).parent('li').find('.show-choice'));
    }
  });

  // -------------------- Regions Change
  $('.cities_select').on('change', function(){
    if($(this).val() != '' && $(this).val() != 'undefined'){
      $.get('/xhrs/districts?id='+$(this).val()+'&node='+$(this).data("node"), function(result){
        $('.district-container').html(result);
        $('.district-container .select2').select2();
      })
    }
  });

  $('form').on('change', '.district_select', function(){
    if($(this).val() != '' && $(this).val() != 'undefined'){
      $.get('/xhrs/villages?id='+$(this).val()+'&node='+$(this).data("node"), function(result){
        $('.village-container').html(result);
        $('.village-container .select2').select2();
      })
    }
  });

  // -------------------- /Regions Change

  $('.provinces_select_tag').on('change', function(){
    if($(this).val() != '' && $(this).val() != 'undefined'){
      $.get('/xhrs/cities?id='+ $(this).val(), function(result){
        $('.city-container').html(result);
        $('.city-container .select2').select2();
      })
    }
  });

  $('form').on('change', '.choice-category-select', function(){
    if($(this).val() != '' && $(this).val() != 'undefined'){
      $.get('/xhrs/sub_categories?id='+$(this).val()+'&node='+$(this).data("node"), function(result){
        $('.sub-category-container').html(result);
        $('.sub-category-container .select2').select2();
      })
    }
  });

  $('form').on('change', '.choice-sub-category-select', function(){
    if($(this).val() != '' && $(this).val() != 'undefined'){
      $.get('/xhrs/brands?id='+$(this).val()+'&node='+$(this).data("node"), function(result){
        $('.brand-container').html(result);
        $('.brand-container .select2').select2();
      })
    }
  });

});

function previewFile(input, imageHeader) {
  var preview = imageHeader[0];
  var file    = input.files[0];
  var reader  = new FileReader();

  reader.onloadend = function () {
    preview.src = reader.result;
  }

  if (file) {
    reader.readAsDataURL(file);
  } else {
    preview.src = "";
  }
}
