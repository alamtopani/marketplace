//= require backend/jquery
//= require jquery_ujs
//= require backend/bootstrap.min
//= require backend/jquery-ui.min
// require backend/fullcalendar.min
//= require backend/jquery.rateit.min
// require backend/jquery.prettyPhoto
//= require backend/jquery.slimscroll.min
//= require backend/jquery.dataTables.min
//= require backend/excanvas.min
//= require backend/jquery.flot
//= require backend/jquery.flot.resize
//= require backend/jquery.flot.pie
//= require backend/jquery.flot.stack
//= require backend/jquery.noty
//= require backend/themes/default
//= require backend/layouts/bottom
//= require backend/layouts/topRight
//= require backend/layouts/top
//= require backend/sparklines
//= require backend/jquery.cleditor.min
//= require backend/jquery.onoff.min
//= require backend/filter
//= require backend/custom
//= require backend/charts
//= require backend/select2
//= require ckeditor/init
//= require ckeditor/config
//= require backend/configurations
//= require cocoon
//= require moment
//= require bootstrap-datetimepicker


$(document).ready(function(){
  $('.datepicker').datetimepicker({
    format: 'YYYY-MM-DD'
  });

  $('.dynamic_select').bind('change', function () {
    var url = $(this).find(':selected').parent('select').data('url');
    if (url) {
        window.location = url;
    }
    return false;
  });

  $(".check_all_box input").click(function(){
    $('input:checkbox').not(this).prop('checked', this.checked);
  });

  $('.check-type-blog').click(function(){
    if(this.value == 'video'){
      $('.section-video').show();
      $('.section-foto').hide();
    }else if(this.value == 'foto'){
      $('.section-foto').show();
      $('.section-video').hide();
    }else{
      $('.section-foto').hide();
      $('.section-video').hide();
    }
  });

  $('.check-type-blog:checked').each(function(){
    if(this.value == 'video'){
      $('.section-video').show();
      $('.section-foto').hide();
    }else if(this.value == 'foto'){
      $('.section-foto').show();
      $('.section-video').hide();
    }else{
      $('.section-foto').hide();
      $('.section-video').hide();
    }
  });
});
