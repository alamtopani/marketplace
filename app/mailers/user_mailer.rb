class UserMailer < ActionMailer::Base
  include ActionView::Helpers::NumberHelper
  helper :application

  default from: 'sukabumipedia@gmail.com'
  default to: 'sukabumipedia@gmail.com'

  # USER MAILER
  def welcome_join(user)
    @setting = WebSetting.first
    @user = user
    mail(to: user.email, subject: 'Selamat datang di SukabumiPedia')
  end

  def send_request_user_to_merchant(user)
    @setting = WebSetting.first
    @user = user
    mail(from: user.email, subject: 'Permohonan Verifikasi - Perubahan akun Member ke Toko/Reseller')
  end

  def send_user_approve_to_verified(user)
    @setting = WebSetting.first
    @user = user
    mail(to: user.email, subject: 'Persetujuan Verifikasi - Akun anda berhasil dirubah ke akun Toko/Reseller')
  end

  # REPORT MAILER

  def send_report(report)
    #@logo = "#{root_url}assets/logo.png"
    @setting = WebSetting.first
    @report = report
    mail(from: report.email, subject: "Di Terima Report - #{report.code}")
  end

  # PRODUCT MAILER
  def send_request_product(product)
    @setting = WebSetting.first
    @product = product
    mail(from: product.user.email, subject: "Permohonan Iklan Baru - #{product.code}")
  end

  def send_product_verified(product)
    @setting = WebSetting.first
    @product = product
    mail(to: product.user.email, subject: "Iklan Berhasil di Verifikasi - #{product.code}")
  end

  def send_product_not_verified(product)
    @setting = WebSetting.first
    @product = product
    mail(to: product.user.email, subject: "Iklan Tidak Terverifikasi - #{product.code}")
  end

  def send_product_featured(product)
    @setting = WebSetting.first
    @product = product
    mail(to: product.user.email, subject: "Perubahan Iklan Menjadi Iklan Terpilih  - #{product.code}")
  end

  def send_guest_book(guest_book)
    @setting = WebSetting.first
    @guest_book = guest_book
    mail(from: @guest_book.email, subject: 'Notifikasi Pengiriman Buku Tamu!')
  end

  def send_comment(comment, url)
    @setting = WebSetting.first
    @user = comment.user
    @comment = comment
    mail(from: @user.email,subject: "Komentar Untuk Iklan Anda!")
  end

end
