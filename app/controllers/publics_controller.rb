class PublicsController < FrontendController
	add_breadcrumb "Home", :root_path

	def home
    @products = Product.featured.verify.latest.limit(12)
	end
end
