class InquiriesController < FrontendController

  def create
    @inquiry = Inquiry.new(permitted_params)

    if current_user
      @inquiry.user_id = current_user.id
      @inquiry.name = current_user.username
      @inquiry.phone = current_user.contact.phone
      @inquiry.email = current_user.email
    end

    respond_to do |format|
      if @inquiry.save
        format.html {redirect_to :back, notice: 'Pesan anda sudah berhasil dikirim!'}
      else
        format.html {redirect_to :back, errors: @inquiry.errors.full_messages}
      end
    end
  end

  def create_instant_message
    @inquiry = Inquiry.new(permitted_params)

    if current_user
      @inquiry.user_id = current_user.id
      @inquiry.name = current_user.username
      @inquiry.phone = current_user.contact.phone
      @inquiry.email = current_user.email
    end
    
    if @inquiry.save
      render json: true
    end
  end

  private

    def permitted_params
      params.require(:inquiry).permit(Permitable.controller(params[:controller]))
    end
end
