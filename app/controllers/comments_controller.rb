class CommentsController < FrontendController
	def create
    @comment = Comment.new(permitted_params)
    @comment.user_id = current_user.id
    if @comment.save
      UserMailer.send_comment(@comment, product_url(@comment.commentable.slug)).deliver if @comment.commentable_type == 'Product'
      redirect_to :back, notice: 'Komentar anda berhasil dikirim!'
    else
      flash[:errors] = @comment.errors.full_messages
      redirect_to :back
    end
	end

  private
    def permitted_params
      params.require(:comment).permit(Permitable.controller(params[:controller]))
    end
end
