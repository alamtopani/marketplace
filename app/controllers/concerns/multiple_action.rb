module MultipleAction
  extend ActiveSupport::Concern

  def do_multiple_act
    if params[:ids].present?
      obj_model = collection.name.singularize.classify.constantize
      collect = obj_model.where("id IN (?)", params[:ids])

      if params[:commit] == 'destroy_all'
        do_destroy_all(collect)
      elsif params[:commit] == 'verified'
        verified_multiple(collect)
      elsif params[:commit] == 'non_verified'
        non_verified_multiple(collect)
      elsif params[:commit] == 'activated'
        activated_multiple(collect)
      elsif params[:commit] == 'non_activated'
        non_activated_multiple(collect)
      elsif params[:commit] == 'featured'
        change_featured_status(collect)
      end
    else
      redirect_to :back, alert: 'Please Choice Selected!'
    end
  end

  def do_destroy_all(collect)
    if collect.destroy_all
      redirect_to :back, notice: 'Successfully Destroyed!'
    else
      redirect_to :back, alert: 'Not Successfully Destroyed!'
    end
  end

  def verified_multiple(collect)
    collect.each do |user|
      UserMailer.send_user_approve_to_verified(user).deliver
      user.verified = true
      user.save
    end
    redirect_to :back, notice: "Verified multiple users successfully change!"
  end

  def non_verified_multiple(collect)
    collect.each do |catalog|
      catalog.verified = false
      catalog.save
    end
    redirect_to :back, notice: "Unverified multiple items successfully change!"
  end

  def activated_multiple(collect)
    collect.each do |catalog|
      UserMailer.send_product_verified(catalog).deliver
      catalog.status = true
      catalog.save
    end
    redirect_to :back, notice: "Activated multiple items successfully change!"
  end

  def non_activated_multiple(collect)
    collect.each do |catalog|
      UserMailer.send_product_not_verified(catalog).deliver
      catalog.status = false
      catalog.save
    end
    redirect_to :back, notice: "Activated multiple items successfully change!"
  end

  def change_featured_status(collect)
    collect.each do |catalog|
      if catalog.featured == false
        UserMailer.send_product_featured(catalog).deliver
        catalog.featured = true
        catalog.save
      else
        catalog.featured = false
        catalog.save
      end
    end

    redirect_to :back, notice: "You featured multiple items successfully change!"
  end

end
