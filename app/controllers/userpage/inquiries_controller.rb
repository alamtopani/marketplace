class Userpage::InquiriesController < Userpage::ApplicationController  
  add_breadcrumb "Dashboard", :userpage_dashboard_path
  add_breadcrumb "Pesan", :collection_path

  def index
    @inquiries = collection.my_messages(current_user.id).roots.latest
    @collection = @inquiries.page(page).per(per_page)
  end

  def show
    add_breadcrumb "Show"
    if resource.present?
      resource.already_read(current_user.id)

      collect = resource.children

      if collect.present?
        collect = collect.where("id IN (?)", collect.pluck(:id)).latest
        resource.already_read_all(collect, current_user.id)

        @comments = collect.page(page).per(per_page)
      end
    end
  end

  def create
    build_resource
    resource.user_id = current_user.id
    create! do |format|
      if resource.errors.empty?
        format.html {redirect_to :back, notice: 'Pesan anda berhasil di kirim!'}
      else
        format.html {redirect_to :back, errors: resource.errors.full_messages}
      end
    end
  end

  def destroy
    @inquiry = Inquiry.find(params[:id])

    respond_to do |format|
      if @inquiry.destroy
        format.html {redirect_to :back, notice: 'Pesan anda tidak berhasil di kirim!'}
      else
        format.html {redirect_to :back, errors: @inquiry.errors.full_messages}
      end
    end
  end
end
