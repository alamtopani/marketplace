class Userpage::ProductsController < Userpage::ApplicationController
  defaults resource_class: Product, collection_name: 'products', instance_name: 'product'

  add_breadcrumb "Dashboard", :userpage_dashboard_path
  add_breadcrumb "Iklan Saya", :collection_path

  def index
    add_breadcrumb "Iklan Tayang"
    @products = current_user.products.latest.verify.search_by(params)
    @collection = @products.page(page).per(per_page)
  end

  def pending
    add_breadcrumb "Iklan Belum Tayang"
    @products = current_user.products.latest.unverify.search_by(params)
    @collection = @products.page(page).per(per_page)
  end

  def archive
    add_breadcrumb "Arsip"
    @products = current_user.products.latest.only_deleted.search_by(params)
    @collection = @products.page(page).per(per_page)
  end

  def new
    add_breadcrumb "Pasang Iklan"
    super
  end

  def edit
    add_breadcrumb "Ubah Iklan"
    super
  end

  def create
    build_resource
    resource.user_id = current_user.id
    create! do |format|
      if resource.errors.empty?
        UserMailer.send_request_product(resource).deliver
        format.html {redirect_to :back, notice: 'Iklan anda berhasil dibuat dan akan segera bisa dilihat setelah di verifikasi oleh Staf Admin Kami, Verifikasi Max 1 x 24 Jam! Terimakasih '}
      else
        format.html {redirect_to :back, error: resource.errors.full_messages}
      end
    end
  end

  def update
    update! do |format|
      if resource.errors.empty?
        format.html {redirect_to collection_path}
      else
        format.html {redirect_to :back, error: resource.errors.full_messages}
      end
    end
  end

  def destroy
    destroy! do |format|
      if resource.errors.empty?
        format.html {redirect_to :back}
      else
        format.html {redirect_to :back, error: resource.errors.full_messages}
      end
    end
  end

  def restore
    resource = collection.with_deleted.find params[:id]
    if resource.restore(recursive: true)
      redirect_to :back, notice: "Iklan #{resource.code} berhasil di kembalikan!"
    else
      redirect_to :back, alert: 'Error'
    end
  end
end