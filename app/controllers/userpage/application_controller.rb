class Userpage::ApplicationController < Backend::ResourcesController
  layout 'user'

  before_filter :authenticate_user!
  protect_from_forgery with: :exception

  def authenticate_user!
    unless current_user.present? && (current_user.member?) || current_user.present? && (current_user.merchant?)
      redirect_to root_path, alert: "Can't Access this page"
    end
  end

  protected
    def per_page
      params[:per_page] ||= 20
    end

    def page
      params[:page] ||= 1
    end
end
