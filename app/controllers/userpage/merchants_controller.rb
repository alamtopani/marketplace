class Userpage::MerchantsController < Userpage::ApplicationController
  defaults resource_class: Merchant, collection_name: 'merchants', instance_name: 'merchant'

  add_breadcrumb "Dashboard", :userpage_dashboard_path
  add_breadcrumb "Personal Data"


  def edit
    add_breadcrumb "Edit"
    super
  end
  
  def update
    update! do |format|
      if resource.errors.empty?
        format.html {redirect_to :back}
      else
        format.html {redirect_to :back, errors: resource.errors.full_messages}
      end
    end
  end

end
