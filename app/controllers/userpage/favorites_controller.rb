class Userpage::FavoritesController < Userpage::ApplicationController

  add_breadcrumb "Dashboard", :userpage_dashboard_path
  add_breadcrumb "Iklan Favorit", :collection_path

  def index
    @favorites = current_user.favorites.only_product.latest
    @collection = @favorites.page(page).per(per_page)
  end

end
