class ProductsController < FrontendController
  add_breadcrumb "Home", :root_path
  add_breadcrumb "Iklan", :products_path

  before_action :prepare_simple_search, only: [:index]

  def index
    categories

    if params[:category].present?
      @category = Category.find_by(name: params[:category])
    end

    @product_all = collection.latest.search_by(params)
    @products = @product_all.page(page).per(per_page)
  end

  def show
    add_breadcrumb "#{resource.title}"

    resource
    @user = resource.user
    @user_products = @user.products.verify.latest.limit(5)
    @profile = @user.profile
    @address = @user.address
    @contact = @user.contact
    @merchant = @user.merchant_info
    impressionist(@product, 'message')

    @favorites_size = @product.favorites.size
    @comments_size = @product.comments.size
    @comments = @product.comments.latest.page(page).per(per_page)

    @product = Product.find params[:id]

    rating(@product.id)
  end

  def new
    add_breadcrumb "Produk Baru"

    @product = Product.new
  end

  def upvote
    @link = Product.find params[:id]
    @success = @link.upvote_by current_user
    if @success
      redirect_to :back, alert: "Anda sudah menyukai iklan ini!"
    else
      redirect_to :back, error: "Anda tidak berhasil menyukai iklan ini!"
    end
  end

  def delete_reviews
    rate = Rate.find(params[:rate_id])
    if rate.delete
      flash[:success] = 'Review anda berhasil di hapus!'
    else
      flash[:error] = rate.errors.full_message
    end
    redirect_to product_path(params[:id])
  end

  private
    def collection
      @products = Product.verify
    end

    def resource
      @product = Product.find params[:id]
    end

    def categories
      @categories = Category.roots.oldest
    end

    def prepare_simple_search
      @cities = Region.roots.alfa.pluck(:name)
    end

    def rating(product_id)
      @reviews = Rate.where(rateable_id: product_id).page(page).per(per_page)
    end
end

