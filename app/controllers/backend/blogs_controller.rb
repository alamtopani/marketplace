class Backend::BlogsController < Backend::ApplicationController
  defaults resource_class: Blog, collection_name: 'blogs', instance_name: 'blog'

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Blog", :collection_path

  include MultipleAction

  def index
    @collection = collection.latest.page(page).per(per_page)
  end

  protected
    def collection
      @collection ||= end_of_association_chain
    end
end
