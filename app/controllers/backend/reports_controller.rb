class Backend::ReportsController < Backend::ApplicationController
  defaults resource_class: Report, collection_name: 'reports', instance_name: 'report'

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Reports", :collection_path

  include MultipleAction

  def index
    @collection = collection.latest.page(page).per(per_page)
  end

  protected
    def collection
      @collection ||= end_of_association_chain.search_by(params)
    end
end
