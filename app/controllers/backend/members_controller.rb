class Backend::MembersController < Backend::ApplicationController
  defaults resource_class: Member, collection_name: 'members', instance_name: 'member'

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Members", :collection_path

  include MultipleAction

  def index
    @collection = collection.latest.page(page).per(per_page)
  end
  
  def update
    update! do |format|
      if resource.errors.empty?
        format.html {redirect_to :back}
      else
        format.html {redirect_to :back, errors: resource.errors.full_messages}
      end
    end
  end

  protected
    def collection
      @collection ||= end_of_association_chain.search_by(params)
    end
end
