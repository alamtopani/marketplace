class Backend::ApplicationController < Backend::ResourcesController
  layout 'backend'

  before_filter :authenticate_admin!, :prepare_count
  protect_from_forgery with: :exception

  def authenticate_admin!
    unless current_user.present? && (current_user.admin?)
      redirect_to root_path, alert: "Can't Access this page"
    end
  end

  protected
    def prepare_count
      @members_size = Member.all.size
      @merchants_size = Merchant.all.size
      @products_size = Product.all.size
    end

    def per_page
      params[:per_page] ||= 20
    end

    def page
      params[:page] ||= 1
    end
end
