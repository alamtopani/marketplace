class Backend::CategoriesController < Backend::ApplicationController
  defaults resource_class: Category, collection_name: 'categories', instance_name: 'category'

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Categories", :collection_path

  include MultipleAction

  def index
    @collection = collection.roots.alfa.page(page).per(per_page)
  end

  def edit
    @collection = resource.children.alfa
  end

  protected
    def collection
      @collection ||= end_of_association_chain
    end
end
