class Backend::ProductsController < Backend::ApplicationController
  defaults resource_class: Product, collection_name: 'products', instance_name: 'product'

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Products", :collection_path

  include MultipleAction

  def index
    @collection = collection.verify
  end

  def pending
    @collection = collection.unverify
  end

  def archive
    @collection = collection.only_deleted
  end

  def create
    build_resource
    create! do |format|
      if resource.errors.empty?
        format.html {redirect_to collection_path}
      else
        flash[:errors] = resource.errors.full_messages
        format.html {redirect_to :back}
      end
    end
  end

  def update
    update! do |format|
      if resource.errors.empty?
        format.html {redirect_to collection_path}
      else
        flash[:errors] = resource.errors.full_messages
        format.html {redirect_to :back}
      end
    end
  end

  def restore
    resource = collection.with_deleted.find params[:id]
    if resource.restore(recursive: true)
      redirect_to :back, notice: "Product code #{resource.code} successfully restore!"
    else
      redirect_to :back, alert: 'Error'
    end
  end

  protected
    def collection
      @collection ||= end_of_association_chain.search_by(params).latest.page(page).per(per_page)
    end
end
