class Backend::TestimonialsController < Backend::ApplicationController
  defaults resource_class: Testimonial, collection_name: 'testimonials', instance_name: 'testimonial'

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Testimonials", :collection_path

  include MultipleAction

  def index
    @collection = collection.latest.page(page).per(per_page)
  end

  protected
    def collection
      @collection ||= end_of_association_chain
    end
end
