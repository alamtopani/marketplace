class Backend::CategoryBlogsController < Backend::ApplicationController
  defaults resource_class: CategoryBlog, collection_name: 'category_blogs', instance_name: 'category_blog'

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Category Blog", :collection_path

  include MultipleAction
  
  def index
    @collection = collection.roots.alfa.page(page).per(per_page)
  end

  def edit
    @collection = resource.children.alfa
  end

  protected
    def collection
      @collection ||= end_of_association_chain
    end
end
