class Backend::RegionsController < Backend::ApplicationController
  defaults resource_class: Region, collection_name: 'regions', instance_name: 'region'

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Regions", :collection_path

  include MultipleAction
  
  def index
    @collection = collection.roots.alfa.page(page).per(per_page)
  end

  def create
    build_resource
    create! do |format|
      if resource.errors.empty?
        format.html {redirect_to collection_path}
      else
        flash[:errors] = resource.errors.full_messages
        format.html {redirect_to :back}
      end
    end

  end


  protected
    def collection
      @collection ||= end_of_association_chain
    end
end
