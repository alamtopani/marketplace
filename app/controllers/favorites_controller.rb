class FavoritesController < FrontendController
	def wishlist
    checking = Favorite.where(favoriteable_id: params[:id], favoriteable_type: params[:type], user_id: current_user.id)

    if checking.present?
      redirect_to :back, alert: "Iklan ini sudah disimpan sebagai iklan favorit anda"
    else
      @wishlist                   = Favorite.new
      @wishlist.user_id           = current_user.id
      @wishlist.favoriteable_id   = params[:id]
      @wishlist.favoriteable_type = params[:type]

      if @wishlist.save
        redirect_to :back, notice: "Berhasil disimpan sebagai iklan favorit anda!"
      else
        redirect_to :back, alert: "Tidak berhasil disimpan sebagai favorit!"
      end
    end
  end
end
