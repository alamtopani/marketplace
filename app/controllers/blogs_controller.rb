class BlogsController < FrontendController
  add_breadcrumb "Home", :blogs_path

  layout 'blog'

  before_action :prepare_action

  def index
    @blogs = Blog.verified.latest
    @breaking = @blogs.last

    @carousel_blogs = @blogs
    @featureds = @blogs.featured
    @headlines = Blog.verified.headline
    @tab_blogs = @blogs.scope_blog
    @gallery_featured = @blogs.foto.last
    @gallery_foto = @blogs
    @gallery_video = @blogs

    @fotos = @blogs.foto
    @videos = @blogs.video
  end

  def show
    add_breadcrumb "#{resource.category_blog.name}"
    add_breadcrumb "#{resource.title}"

    resource
    prepare_quick_count(resource)

    @blogs = Blog.verified.bonds.where("category_id =?", resource.category_id).scope_blog.latest
    @featureds = @blogs.featured
    @headlines = Blog.verified.headline

    @comments = @blog.comments.latest.page(page).per(per_page)
  end

  def category
    add_breadcrumb "Blog"
    add_breadcrumb "#{category_blog.name}"
    category_blog
    @blogs = category_blog.blogs.scope_blog.verified.latest

    blogs = Blog.verified.latest
    @featureds = blogs.featured
    @headlines = Blog.verified.headline
  end

  def results
    add_breadcrumb "Blog"
    add_breadcrumb "Results"

    blogs = Blog.verified.latest
    @blogs = blogs.search_by(params).scope_blog
    @featureds = blogs.featured
    @headlines = Blog.verified.headline
  end

  private
    def resource
      @blog = Blog.find params[:id]
    end

    def category_blog
      @category_blog = CategoryBlog.find params[:id]
    end

    def prepare_action
      @category_blogs = CategoryBlog.roots.latest.alfa
    end

    def prepare_quick_count(resource)
      resource = Blog.find(resource)
      resource.view_count = resource.view_count+1
      resource.save
    end
end
