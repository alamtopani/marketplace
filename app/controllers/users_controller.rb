class UsersController < FrontendController
  add_breadcrumb "Home", :root_path
  add_breadcrumb "Toko"
  
  def show
    add_breadcrumb "#{params[:id]}"
    
    @user = User.find params[:id]
    @products = @user.products.verify.latest.search_by(params).page(page).per(per_page)

    @favorites_size = @user.favorites.size
    impressionist(@user, 'message')
  end

  def upvote
    @link = User.find params[:id]
    @success = @link.upvote_by current_user
    if @success
      redirect_to :back, alert: "Anda sudah menyukai halaman ini!"
    else
      redirect_to :back, error: "Anda tidak berhasil menyukai halaman ini!"
    end
  end
end