class XhrsController < FrontendController

  def cities
    province = Region.find_by(name: params[:id])
    @cities = province ? province.children : Region.none
    render layout: false
  end

  def districts
    city = Region.find_by(name: params[:id])
    @districts = city ? city.children : Region.none
    render layout: false
  end

  def villages
    subdistrict = Region.find_by(name: params[:id])
    @villages = subdistrict ? subdistrict.children : Region.none
    render layout: false
  end

  def sub_categories
    @sub_categories = Category.find(params[:id]).children
    render layout: false
  end

  def brands
    @brands = Category.find(params[:id]).children
    render layout: false
  end
end
