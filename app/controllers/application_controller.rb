class ApplicationController < ActionController::Base
  include TheUser::DeviseFilter
  protect_from_forgery with: :exception
  before_filter :web_setting
  before_filter :prepare_checked
  before_filter :set_current_user

  protected
    def per_page
      params[:per_page] ||= 36
    end

    def page
      params[:page] ||= 1
    end

    def web_setting
      @landing_pages = LandingPage.activated.oldest
      @categories = Category.roots.latest
      @setting = WebSetting.first
    end

    def prepare_checked
      if current_user
        @latest_inquiries = Inquiry.available.received(current_user.id)
      end
    end

    def set_current_user
      User.current = current_user
    end
end
