class Blog < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: [:slugged, :finders]

  after_initialize :populate_galleries

  include ScopeBased
  include TheBlog::BlogScope
  include TheBlog::BlogSearching
  belongs_to :category_blog, foreign_key: 'category_id'
  belongs_to :user, foreign_key: 'user_id'

  has_many :galleries, as: :galleriable, dependent: :destroy
  accepts_nested_attributes_for :galleries, reject_if: :all_blank, allow_destroy: true

  scope :foto, -> {where(type_blog: 'foto')}
  scope :video, -> {where(type_blog: 'video')}

  scope :verified, -> {where(status: true)}
  scope :featured, -> {where(featured: true)}
  scope :headline, -> {order(view_count: :desc)}
  scope :scope_blog, -> {where(type_blog: 'blog')}
  scope :bonds,-> {
    eager_load(:category_blog)
  }

  has_attached_file :cover, styles: {
                      medium:   '500x500>',
                      thumb:    '128x128>',
                    },
                    default_url: 'no-image.png'

  validates_attachment :cover, content_type: {
    content_type: ["image/jpg", "image/jpeg", "image/png"]
  }

  def active?
    if self.status == true
      return "<i class='fa fa-check'></i>Published".html_safe
    else
      return "<i class='fa fa-exclamation'></i>Not Published".html_safe
    end
  end

  def featured?
    if self.featured == true
      return "[ <i class='fa fa-check'></i>Featured ]".html_safe
    else
      return "".html_safe
    end
  end

  def build_image(media_title, position=nil)
    gallery = self.galleries.build({title: media_title})
    gallery.position = position
  end

  def video?
    self.type_blog == 'video'
  end

  def foto?
    self.type_blog == 'foto'
  end

  def blog?
    self.type_blog == 'blog'
  end

  private
    def populate_galleries
      if self.galleries.length < 6
        [
          'Cover',
          'Image1',
          'Image2',
          'Image3',
          'Image4',
          'Image5',
        ].each_with_index do |media_title, index|
          _galery = self.galleries.select{|g| g.title.to_s.downcase == media_title.downcase}.first
          unless _galery
            self.build_image(media_title, index+1)
          else
            _galery.position = index+1
          end
        end
      end if self.new_record?
    end
end
