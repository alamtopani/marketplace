class WebSetting < ActiveRecord::Base
  include ScopeBased

  after_initialize :after_initialized

  has_many :galleries, as: :galleriable, dependent: :destroy
  accepts_nested_attributes_for :galleries, reject_if: :all_blank, allow_destroy: true

  has_one :faq, as: :faqable, dependent: :destroy
  accepts_nested_attributes_for :faq, reject_if: :all_blank, allow_destroy: true

  has_many :payments, as: :paymentable, dependent: :destroy
  accepts_nested_attributes_for :payments, reject_if: :all_blank, allow_destroy: true

  has_many :shipping_methods, as: :shipping_methodable, dependent: :destroy
  accepts_nested_attributes_for :shipping_methods, reject_if: :all_blank, allow_destroy: true

  has_attached_file :favicon, styles: {
                      small:    '300>',
                      thumb:    '64x64>'
                    },
                    default_url: 'no-image.png'

  validates_attachment :favicon, content_type: {
    content_type: ["image/jpg", "image/jpeg", "image/png"]
  }

  has_attached_file :logo, styles: {
                      small:    '300>',
                      thumb:    '64x64>'
                    },
                    default_url: 'no-image.png'

  validates_attachment :logo, content_type: {
    content_type: ["image/jpg", "image/jpeg", "image/png"]
  }

  private
    def after_initialized
      self.faq = Faq.new if self.faq.blank?
    end

end
