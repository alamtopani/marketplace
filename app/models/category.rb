class Category < ActiveRecord::Base
	include Tree
	extend FriendlyId
  friendly_id :name, use: [:slugged, :finders]

  has_many :category_products, class_name: "Category", foreign_key: 'category_id'
  has_many :sub_category_products, class_name: "Category", foreign_key: 'sub_category_id'
  has_many :brand_products, class_name: "Category", foreign_key: 'brand_id'

	include ScopeBased
  default_scope {order(name: :asc)}
  
	scope :alfa, -> {order("name ASC")}

  has_attached_file :file, styles: { 
                      medium:   '256x256>', 
                      small:    '128x128>',
                      thumb:    '64x64>'
                    }, 
                    default_url: 'no-image.png'
                                    
  validates_attachment :file, content_type: { 
    content_type: ["image/jpg", "image/jpeg", "image/png"] 
  }

  def active?
    if self.status == true
      return "<i class='fa fa-check'></i>Published".html_safe
    else
      return "<i class='fa fa-exclamation'></i>Not Published".html_safe
    end
  end
end
