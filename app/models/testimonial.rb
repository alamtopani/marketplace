class Testimonial < ActiveRecord::Base
	belongs_to :user

  include ScopeBased
  scope :verified, -> {where(status: true)}

	def status?
    return '<span class="label label-success">Already Verified</span>'.html_safe if self.status == true
    return '<span class="label label-warning">Not Yet Verified</span>'.html_safe if self.status == false
  end
end
