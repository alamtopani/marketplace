class Faq < ActiveRecord::Base
  belongs_to :faqable, polymorphic: true
end
