module TheProduct
	module ProductScope
		extend ActiveSupport::Concern

		included do
			belongs_to :user, foreign_key: 'user_id'
		  belongs_to :parent_category, class_name: 'Category', foreign_key: 'category_id'
		  belongs_to :sub_category, class_name: 'Category', foreign_key: 'sub_category_id'
		  belongs_to :brand, class_name: 'Category', foreign_key: 'brand_id'

			has_many :comments, as: :commentable, dependent: :destroy
			accepts_nested_attributes_for :comments, reject_if: :all_blank, allow_destroy: true

		  has_many :galleries, as: :galleriable, dependent: :destroy
		  accepts_nested_attributes_for :galleries, reject_if: :all_blank, allow_destroy: true

		  has_many :favorites, as: :favoriteable, dependent: :destroy
		  accepts_nested_attributes_for :favorites, reject_if: :all_blank, allow_destroy: true

		  has_one :address, as: :addressable, dependent: :destroy
	    accepts_nested_attributes_for :address, reject_if: :all_blank, allow_destroy: true

			scope :latest, -> {order(created_at: :desc)}
			scope :oldest, -> {order(created_at: :asc)}

		  scope :low_to_high, -> {order("products.price ASC")}
		  scope :high_to_low, -> {order("products.price DESC")}

		  scope :last_day, -> {where('products.created_at >= :last_days_ago', :last_days_ago  => Time.now - 1.days)}
		  scope :last_week, -> {where('products.created_at >= :last_weeks_ago', :last_weeks_ago  => Time.now - 7.days)}
		  scope :last_month, -> {where('products.created_at >= :last_months_ago', :last_months_ago  => Time.now - 30.days)}
		  scope :last_year, -> {where('products.created_at >= :last_years_ago', :last_years_ago  => Time.now - 360.days)}

		  scope :verify, -> {where("products.status = ?", true)}
		  scope :unverify, -> {where("products.status = ?", false)}
		  scope :featured, -> {where("products.featured = ?", true)}

		  scope :new_product, ->{where("products.category = ?", 'barang_baru')}
		  scope :used_product, ->{where("products.category = ?", 'barang_bekas')}

		  scope :bonds, -> {
		  	eager_load({ user: [:profile, :merchant_info]}, :address, :parent_category, :sub_category)
		  }

		end
	end
end
