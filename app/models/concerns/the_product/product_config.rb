module TheProduct
  module ProductConfig
    extend ActiveSupport::Concern

    def active?
      if self.status == true
        return "<i class='fa fa-check'></i>Published".html_safe
      else
        return "<i class='fa fa-exclamation'></i>Not Published".html_safe
      end
    end

    def featured?
      if self.featured == true
        return "[Featured]".html_safe
      end
    end

    def set_expired_time?
      if self.started_at == nil && self.expired_at == nil
        self.started_at = Date.today
        self.expired_at = Date.today + 30.days
      end
    end

    def is_expired?
      self.expired_at < Date.today rescue nil
    end

    def build_image(media_title, position=nil)
      gallery = self.galleries.build({title: media_title})
      gallery.position = position
    end
  end
end
