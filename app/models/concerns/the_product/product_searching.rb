module TheProduct
  module ProductSearching
    extend ActiveSupport::Concern

    module ClassMethods
      def by_keywords(_key)
        return if _key.blank?
        query_opts = [
          "LOWER(products.code) LIKE LOWER(:key)",
          "LOWER(products.title) LIKE LOWER(:key)",
          "LOWER(users.username) LIKE LOWER(:key)",
          "LOWER(users.email) LIKE LOWER(:key)",
          "LOWER(profiles.full_name) LIKE LOWER(:key)",
          "LOWER(merchant_infos.title) LIKE LOWER(:key)"
        ].join(' OR ')
        where(query_opts, {key: "%#{_key}%"} )
      end

      def by_condition(_condition)
        return if _condition.blank?
        where("products.category =?", _condition)
      end

      def by_category(_category)
        return if _category.blank?
        where("LOWER(categories.name) LIKE LOWER(:key)", key: "%#{_category}%")
      end

      def by_sub_category(_sub_category)
        return if _sub_category.blank?
        where("products.sub_category_id =?", _sub_category)
      end

      def by_brand(_brand)
        return if _brand.blank?
        where("products.brand_id =?", _brand)
      end

      def by_city(_city)
        return if _city.blank?
        where("addresses.city =?", _city)
      end

      def by_district(_district)
        return if _district.blank?
        where("addresses.district =?", _district)
      end

      def sort_by(_sort)
        return if _sort.blank?
        self.send(_sort)
      end

      def price_range(_price_range)
        return if _price_range.blank?
        price = _price_range.split(";")
        where("products.price >=?", price[0].to_i).where("products.price <=?", price[1].to_i)
      end

      def search_by(options={})
      
        results = bonds

        if options[:key].present?
          results = results.by_keywords(options[:key])
        end

        if options[:condition].present?
          results = results.by_condition(options[:condition])
        end

        if options[:category].present?
          results = results.by_category(options[:category])
        end

        if options[:sub_category].present?
          category_id = Category.find_by(name: options[:sub_category]).id
          results = results.by_sub_category(category_id)
        end

        if options[:brand].present?
          brand_id = Category.find_by(name: options[:brand]).id
          results = results.by_brand(brand_id)
        end

        if options[:city].present?
          results = results.by_city(options[:city])
        end

        if options[:district].present?
          results = results.by_district(options[:district])
        end

        if options[:sort_by].present?
          results = results.sort_by(options[:sort_by])
        end

        if options[:price_range].present?
          results = results.price_range(options[:price_range])
        end

        return results
      end

    end
  end
end
