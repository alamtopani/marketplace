module TheUser
	module UserConfig
		extend ActiveSupport::Concern

		included do
			devise  :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, #:confirmable,
	         		:validatable, :lockable, :timeoutable, :omniauthable, omniauth_providers: [:facebook], :authentication_keys => [:login]

	    after_initialize :after_initialized

	    has_one :profile, foreign_key: 'user_id', dependent: :destroy
	    accepts_nested_attributes_for :profile, reject_if: :all_blank

	    has_one :address, as: :addressable, dependent: :destroy
	    accepts_nested_attributes_for :address, reject_if: :all_blank

	    has_one :contact, as: :contactable, dependent: :destroy
	    accepts_nested_attributes_for :contact, reject_if: :all_blank

	    has_one :merchant_info, foreign_key: 'user_id', dependent: :destroy
  		accepts_nested_attributes_for :merchant_info, reject_if: :all_blank

	    has_many :products, foreign_key: 'user_id', dependent: :destroy
	    accepts_nested_attributes_for :products, reject_if: :all_blank

	    has_many :inquiries, foreign_key: 'user_id', dependent: :destroy
		  accepts_nested_attributes_for :inquiries, reject_if: :all_blank

		  has_many :my_inquiries, foreign_key: 'owner_id', class_name: 'Inquiry', dependent: :destroy
		  accepts_nested_attributes_for :my_inquiries, reject_if: :all_blank

	    has_many :comments, foreign_key: 'user_id', dependent: :destroy
	    accepts_nested_attributes_for :comments, reject_if: :all_blank

		  has_many :favorites, foreign_key: 'user_id', dependent: :destroy
		  accepts_nested_attributes_for :favorites, reject_if: :all_blank

		  has_many :testimonials, foreign_key: 'user_id', dependent: :destroy
		  accepts_nested_attributes_for :testimonials, reject_if: :all_blank

		  has_one :subscribe, foreign_key: 'email', dependent: :destroy
		  accepts_nested_attributes_for :subscribe, reject_if: :all_blank		

		  has_many :blogs, foreign_key: 'user_id', dependent: :destroy
		  accepts_nested_attributes_for :blogs, reject_if: :all_blank

		  validates_uniqueness_of :username, :email
		  before_save :downcase_username, :prepare_code

		  scope :member, ->{ where(type: 'Member') }
		  scope :admin, ->{ where(type: 'Admin') }
		  scope :featured, ->{ where(featured: true) }

		  scope :latest, -> {order(created_at: :desc)}
			scope :oldest, -> {order(created_at: :asc)}

			scope :bonds, -> { eager_load(:profile, :address) }

		end

		#--------------- ALL TYPE USER --------------------
		def admin?
			self.type == "Admin"
		end

		def member?
			self.type == "Member"
		end

		def merchant?
			self.type == "Merchant"
		end

		#--------------- ALL STATUS USER --------------------
		def verified?
	  	if self.verified == true
	  		return "<i class='fa fa-check-circle-o'></i> Terverifikasi".html_safe
		  else
		  	return "<i class='fa fa-warning'></i> Tidak Terverifikasi".html_safe
		  end
	  end

	  def featured?
	  	if self.featured == true
	  		return "<i class='fa fa-check'></i> Featured".html_safe
		  else
		  	return "<i class='fa fa-warning'></i> Not Featured".html_safe
		  end
	  end

	  def featured_status
	    self.featured ? 'Featured' : 'Un Featured'
	  end

	  #--------------- ALL ACTION USER ----------------------

		def change_featured_status!
	    if self.featured
	      self.featured = false
	      self.save
	    elsif !self.featured
	      self.featured = true
	      self.save
	    end
	  end

	 	#--------------- ALL COUNT USER ----------------------
	  def favorites_size
	  	favories_count = Favorite.only_user.where(favoriteable_id: self.id).size
	  end

	 	def products_size
	 		products_count = self.products.size
	 	end

		protected
			#--------------- GENERATE CODE ID USER ----------------------
			def prepare_code
				self.code = SecureRandom.hex(3) if self.code.blank?
			end

			def after_initialized
				self.profile = Profile.new if self.profile.blank?
				self.address = Address.new if self.address.blank?
				self.contact = Contact.new if self.contact.blank?
      	self.merchant_info = MerchantInfo.new if self.merchant_info.blank?
			end

			def downcase_username
				self.username = self.username.downcase if username_changed?
			end
	end
end
