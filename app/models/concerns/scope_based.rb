module ScopeBased
  extend ActiveSupport::Concern

  included do
    scope :oldest, -> {order(created_at: :asc)}
    scope :latest, -> {order(created_at: :desc)}
    scope :updatest, -> {order(updated_at: :desc)}
  end
end
