class Favorite < ActiveRecord::Base
	belongs_to :user, foreign_key: 'user_id'
  belongs_to :favoriteable, polymorphic: true

	include ScopeBased

  scope :only_product, ->{where(favoriteable_type: 'Product')}
  scope :only_user, ->{where(favoriteable_type: 'User')}
end
