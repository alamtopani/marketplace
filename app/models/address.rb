class Address < ActiveRecord::Base
	belongs_to :addressable, polymorphic: true

  before_save :prepare_save

  def place_info
    [address, postcode, village, district, city].select(&:'present?').join(', ')
  end

  def simple_place_info
    [district, city].select(&:'present?').join(', ')
  end

  private
    def prepare_save
      self.province = 'Jawa Barat'
    end
end
