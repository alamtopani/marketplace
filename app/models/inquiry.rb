class Inquiry < ActiveRecord::Base
  belongs_to :user, foreign_key: 'user_id'
  belongs_to :owner, class_name: 'Inquiry', foreign_key: 'owner_id'
  belongs_to :inquiriable, polymorphic: true

  include ScopeBased
  include Tree

  scope :available, ->{where(status: false)}

  def already_read(user_id)
    if self.owner_id == user_id
      self.status = true
      self.save
    end
  end

  def already_read_all(collect, user_id)
    collect.each do |m|
      if m.owner_id == user_id
        m.status = true
        m.save
      end
    end
  end

  def subject?
    if self.inquiriable_id.present?
      return "Pesan Berkaitan Dengan <b>(#{self.try(:inquiriable).try(:title) || 'Iklan Sudah di Hapus'})</b>".html_safe
    else
      return "Pesan Singkat"
    end
  end

  def reply(user_id)
    if self.user_id == user_id
      return '<i class="fa fa-mail-reply"></i>'.html_safe
    end
  end

  class << self
    def received(user_id)
      where("inquiries.owner_id = ?", user_id).latest
    end

    def my_messages(user_id)
      where("inquiries.user_id = :member_id OR inquiries.owner_id = :member_id", { member_id: user_id }).latest
    end
  end
end
