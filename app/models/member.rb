class Member < User
  ratyrate_rater
  extend FriendlyId
  friendly_id :username, use: [:slugged, :finders]
  
  default_scope {where(type: 'Member')}
end