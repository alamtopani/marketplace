class Report < ActiveRecord::Base
  belongs_to :reportable, polymorphic: true
  before_save :prepare_code

  include ScopeBased
  include TheReport::ReportSearching

  validates_uniqueness_of :code, if: 'self.code.present?'

  scope :bonds, ->{ }

  def status?
    if self.status == true
      return "<span class='btn btn-default btn-xs'>Already Checked</span>".html_safe
    else
      return "<span class='btn btn-danger btn-xs'>New</span>".html_safe
    end
  end

  private
    def prepare_code
      self.code = SecureRandom.hex(3) if self.code.blank?
    end
end
