class User < ActiveRecord::Base
  extend FriendlyId
  friendly_id :username, use: [:slugged, :finders]
  after_create :send_welcome_mail
  
  is_impressionable
  acts_as_votable
  include TheUser::UserConfig
  include TheUser::UserAuthenticate
  include TheUser::UserSearching
  include ScopeBased
  
  def score
    self.get_upvotes.size
  end

  def self.current
    Thread.current[:user]
  end
  
  def self.current=(user)
    Thread.current[:user] = user
  end

  def the_name?
    self.username || self.profile.full_name
  end

  private
    def send_welcome_mail
      UserMailer.welcome_join(self).deliver
    end
end
