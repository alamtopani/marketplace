class MerchantInfo < ActiveRecord::Base
  belongs_to :merchant, foreign_key: 'user_id'

  has_attached_file :banner, styles: { 
                      large:    '512x512>', 
                      medium:   '256x256>'
                    }, 
                    default_url: 'no-image.png'
                                    
  validates_attachment :banner, content_type: { 
    content_type: ["image/jpg", "image/jpeg", "image/png"] 
  }
end
