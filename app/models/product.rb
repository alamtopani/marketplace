class Product < ActiveRecord::Base
	extend FriendlyId
  friendly_id :title, use: [:slugged, :finders]

  is_impressionable
  acts_as_votable
  acts_as_paranoid

  include Ratyrate::Model
  ratyrate_rateable 'happiness'
  
  after_initialize :after_initialized, :populate_galleries
  before_create :prepare_code

  include TheProduct::ProductScope
  include TheProduct::ProductConfig
  include TheProduct::ProductSearching
  include ScopeBased

  def score
    self.get_upvotes.size
  end

  private
    def prepare_code
      self.code = SecureRandom.hex(3) if self.code.blank?
    end

    def after_initialized
      self.address = Address.new if self.address.blank?
    end

    def populate_galleries
      if self.galleries.length < 6
        [
          'Cover',
          'Image1',
          'Image2',
          'Image3',
          'Image4',
          'Image5',
        ].each_with_index do |media_title, index|
          _galery = self.galleries.select{|g| g.title.to_s.downcase == media_title.downcase}.first
          unless _galery
            self.build_image(media_title, index+1)
          else
            _galery.position = index+1
          end
        end
      end if self.new_record?
    end
end
