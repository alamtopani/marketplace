class CategoryBlog < ActiveRecord::Base
  include Tree
  extend FriendlyId
  friendly_id :name, use: [:slugged, :finders]

  has_many :blogs, foreign_key: 'category_id'

  include ScopeBased
  default_scope {order(name: :asc)}
  
  scope :alfa, -> {order("name ASC")}

  has_attached_file :cover, styles: { 
                      medium:   '500x500>', 
                      thumb:    '128x128>',
                    }, 
                    default_url: 'no-image.png'
                                    
  validates_attachment :cover, content_type: { 
    content_type: ["image/jpg", "image/jpeg", "image/png"] 
  }

end
