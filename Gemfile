source 'https://rubygems.org'
gem 'rails', '4.2.3'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.1.0'
gem 'jquery-rails'
gem 'turbolinks'
gem 'jbuilder', '~> 2.0'
gem 'sdoc', '~> 0.4.0', group: :doc

gem 'pg'
gem 'ancestry'                        # Table Tree tool
gem 'inherited_resources'             # DRY for CRUD methods in controller
gem 'kaminari'                        # pagination
gem 'breadcrumbs_on_rails'
gem 'acts_as_list'
gem 'friendly_id'
gem 'impressionist'
gem 'enumerate_it'
gem 'validate_url'
gem 'protokoll'
gem 'erd'
gem 'rubyzip'
gem 'redis-session-store'
gem 'scoped_search'
gem 'activerecord-session_store', github: 'rails/activerecord-session_store'
gem 'cocoon'                          # Dynamic nested forms using jQuery
gem 'httparty'
gem 'wash_out'
gem 'htmlentities'
gem 'jquery-rails'
gem "recaptcha", :require => "recaptcha/rails"
gem 'the_role', '~> 2.5.2'
gem 'the_role_bootstrap3_ui'
gem 'exception_notification', '~> 4.0.1'
gem 'figaro'
gem 'roo'
gem 'roo-xls'
gem 'route_translator'
gem 'sinatra', '>= 1.3.0', :require => nil
gem 'sidekiq'
gem 'capistrano-sidekiq'
gem 'autonumeric-rails'
gem 'dragonfly'
gem 'htmlcompressor'

#A PDF generation plugin for Ruby on Rails
gem 'wicked_pdf'
gem 'wkhtmltopdf-binary'
gem 'country_select'
gem 'omniauth'
gem 'omniauth-facebook'
gem 'devise'
gem 'paperclip', '~> 4.2.0'
gem 'delayed_paperclip'
gem 'paperclip-watermark'
gem 'aws-sdk'
gem 'mini_magick'
gem 'ckeditor'
gem 'rails-timeago', '~> 2.0'
gem 'faker'
gem "select2-rails"
gem 'dynamic_sitemaps'
gem 'acts_as_votable', '~> 0.10.0'
gem 'rails4-autocomplete'
gem 'searchkick'
gem 'ratyrate'
gem 'shareable'
gem 'paypal-sdk-merchant'
gem "cancan"
gem "lazyload-rails"
gem 'bootstrap-sass', '~> 3.3.5'
gem 'slim-rails'
gem 'font-awesome-sass'
gem 'momentjs-rails', '>= 2.9.0'
gem 'bootstrap3-datetimepicker-rails', '~> 4.15.35'
gem 'google-api-client'
gem 'ratyrate'
gem 'ahoy_matey'
gem 'groupdate'
gem 'chartkick'
gem "paranoia", "~> 2.0"

group :development, :test do
  gem 'byebug'
  gem 'binding_of_caller'
  gem 'quiet_assets'
  gem 'letter_opener'
  gem 'better_errors'
  gem 'annotate'                        # Schema to model
  gem 'rspec-rails'
  gem 'factory_girl'
  gem 'shoulda-matchers', require: false
  gem 'database_cleaner'
  gem "sqlite3", "~> 1.3.7", :require => "sqlite3"
  gem 'web-console', '~> 2.0'
  gem 'spring'
end

group :production, :staging do
  gem 'unicorn'
  gem 'rails_12factor'
  gem 'capistrano', '~> 3.1.0'
  gem 'capistrano-rails', '~> 1.1.0'
  gem 'capistrano-bundler'
  gem 'capistrano-rbenv', "~> 2.0" 
  gem 'capistrano3-unicorn', "~> 0.1.1" 
  gem 'rack-cache', :require => 'rack/cache'
  gem 'capistrano-ssh-doctor', '~> 1.0'
end

gem 'nokogiri', group: [:development, :production]
gem 'rest-client', '~> 1.6.7', group: [:development, :production]

