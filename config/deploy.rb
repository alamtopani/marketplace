# config valid only for Capistrano 3.1
lock '3.1.0'

set :application, 'oshop'
set :repo_url, 'git@gitlab.com:alamtopani/marketplace.git'
set :scm, :git
set :deploy_to, '/home/oshop/www'

set :format, :pretty
set :log_level, :info

set :linked_files, %w{config/database.yml}
set :linked_dirs, %w{bin log tmp vendor/bundle public/system}

set :rbenv_path, '/home/oshop/.rbenv'

set :default_env, {
   path: "/home/oshop/.rbenv/shims:/home/oshop/.rbenv/bin:$PATH"
 }

set :keep_releases, 3

# set :assets_prefix, 'prestige'

namespace :deploy do

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      invoke 'unicorn:restart'
    end
  end

  after :finishing, 'deploy:cleanup'
end

after 'deploy:publishing', 'deploy:restart'

