Rails.application.routes.draw do
  devise_for :users,
    controllers: {
      omniauth_callbacks: 'omniauth_callbacks',
      registrations: 'registrations',
      sessions: 'sessions',
      confirmations: "confirmations"
    },
    path_names: {
      sign_in:  'login',
      sign_out: 'logout',
      sign_up:  'register'
    }

  root 'publics#home'

  # get 'contact', to: 'publics#contact', as: 'contact'

  resources :products do
    collection do
      get :auction
      get :featureds, to: 'products#featured'
      get 'wishlist/:id', to: 'products#wishlist', as: 'wishlist'
      post :checkout_bid
    end
    member do
      match :upvote, path: 'like', via: [:get, :post, :put]
      get 'reviews', to: 'products#reviews'
      delete 'reviews/:rate_id', to: 'products#delete_reviews', as: 'delete_rate'
      post 'reviews', to: 'reviews#create'
    end
  end
  resources :favorites do
    collection do
      get :wishlist
    end
  end
  resources :subscribes
  resources :landing_pages
  resources :comments
  resources :reports, only: [:create]
  resources :inquiries do
    collection do
      post :create_instant_message
    end
  end
  resources :guest_books
  resources :users do
    member do
      match :upvote, path: 'like', via: [:get, :post, :put]
    end
  end
  resources :blogs do
    member do
      get :category
    end
    collection do 
      get :results
    end
  end
  resources :xhrs do
    collection do
      get :cities
      get :districts
      get :villages
      get :sub_categories
      get :brands
    end
  end

  namespace :backend do
    get 'dashboard', to: 'home#dashboard', as: 'dashboard'
    resources :admins
    resources :members do
      collection do
        post :do_multiple_act
      end
    end
    resources :merchants do
      collection do
        post :do_multiple_act
      end
    end
    resources :categories do
      collection do
        post :do_multiple_act
      end
    end
    resources :products do
      collection do
        get :pending
        get :archive
        post :do_multiple_act
      end
      member do
        get :restore
      end
    end
    resources :regions do
      collection do
        post :do_multiple_act
      end
    end
    resources :reports do
      collection do
        post :do_multiple_act
      end
    end
    resources :comments do
      collection do
        post :do_multiple_act
      end
    end
    resources :landing_pages do
      collection do
        post :do_multiple_act
      end
    end
    resources :subscribes do
      collection do
        post :do_multiple_act
      end
    end
    resources :testimonials do
      collection do
        post :do_multiple_act
      end
    end
    resources :blogs do
      collection do
        post :do_multiple_act
      end
    end
    resources :category_blogs do
      collection do
        post :do_multiple_act
      end
    end
    resources :web_settings
  end

  namespace :userpage do
    get 'dashboard', to: 'home#dashboard', as: 'dashboard'
    resources :members, only: [:edit, :update, :show]
    resources :merchants, only: [:edit, :update, :show]
    resources :products do
      collection do
        get :pending
        get :archive
      end
      member do
        get :restore
      end
    end
    resources :favorites
    resources :inquiries
  end
end
