role :app, %w{oshop@63.142.252.102}
role :web, %w{oshop@63.142.252.102}
role :db,  %w{oshop@63.142.252.102}

set :stage, :production

# Replace 127.0.0.1 with your server's IP address!
server '63.142.252.102', user: 'oshop', roles: %w{web app}

set :rbenv_type,     :system
set :rbenv_ruby,     '2.2.2'
set :rbenv_prefix,   "RBENV_ROOT=#{fetch(:rbenv_path)} RBENV_VERSION=#{fetch(:rbenv_ruby)} #{fetch(:rbenv_path)}/bin/rbenv exec"
set :rbenv_map_bins, %w{rake gem bundle ruby rails}
set :rbenv_roles,    :all

set :deploy_to,       "/home/oshop/www"
set :rails_env,       "production"
set :branch,          "ongkir_auto"

set :unicorn_config_path, "/home/oshop/www/current/config/unicorn.rb"
