Rails.application.config.assets.version = '1.0'
Rails.application.config.assets.precompile += %w( backend.css )
Rails.application.config.assets.precompile += %w( backend.js )
Rails.application.config.assets.precompile += %w( ckeditor/* )
Rails.application.config.assets.precompile += %w( blog.css )
Rails.application.config.assets.precompile += %w( blog.js )


